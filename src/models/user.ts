import mongoose from "mongoose";

const Schema = mongoose.Schema

var userSchema = new Schema({
   name: String,
   age: Number,
})

export default module.exports = mongoose.model("User", userSchema)