import { NextFunction, Request, RequestHandler, Response } from "express";
import User from '../models/user';

export default class UserController {

    static isMayor = (req: Request, res: Response, next: any) => {
        if (req.body.age >= 18)
            req.body.isMayor = true
        else
            req.body.isMayor = false
        next()
    }

    static all = (req: Request, res: Response, next: NextFunction) => {
        // Get all users from DB and append to req.body
        User.find().lean()
            .then((users: any) => {
                req.body.users = users
                next()
            })
            .catch((err: any) => {
                console.log(err)
            })

    }

    static create = (req: any, res: any, next: any) => {
        const user = new User({ name: req.body.name, age: req.body.age })

        user.save()

        next()
    }

    static view = (viewName: string) => (req: any, res: Response<any>) => {
        res.render(viewName, { data: req.body })
    }
}



// export default exports