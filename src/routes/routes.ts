import express from "express"
const router = express.Router()
import path from "path"
import UserController from "../controllers/user"

// Middleware to show data request
router.use((req, res, next) => {
   console.log("/" + req.method);
   next()
})

// Analize the route and the protocol and reacts to them
router.get("/", (req, res) => {
   res.render("home")
})

router.post("/edad", UserController.isMayor, UserController.create, UserController.view("edad"))

router.post("/user", UserController.create)

router.get("/users", UserController.all, UserController.view("users"))

// module.exports = router
export default router