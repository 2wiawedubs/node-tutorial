import express, { ErrorRequestHandler, Response } from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import routes from './routes/routes';
import { env } from 'process';
import { engine } from 'express-handlebars';

dotenv.config()

const app = express()
const PORT = env.PORT
const MONGO_DB = env.MONGO_DB

const path = __dirname + "/views/"

app.engine("handlebars", engine())

app.set("view engine", "handlebars")
app.set("views", "./views")

app.use(express.static(path))
app.use(express.urlencoded({ extended: true }))
// Allow get data in json Type
app.use(express.json())


app.use("/", routes)


app.listen(PORT, () => {
   console.log(`Example app listening on port ${PORT}`);
   mongoose.connect(
      MONGO_DB,
      // @ts-ignore
      { useUnifiedTopology: true, useNewUrlParser: true },
   )
      .then(res => {
         console.log(`Succesfully conected to database: ${res.connection["name"]}`);
      })
      .catch(err => {
         console.error(err);
      })
})